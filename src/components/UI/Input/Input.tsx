import * as React from "react";

export interface Props {
  type: string;
  name: string;
  placeholder?: string;
  label: string;
  message?: string;
  changeHandler: (event: React.ChangeEvent<HTMLInputElement>) => void;
}

const input: React.FunctionComponent<Props> = ({
  type,
  name,
  placeholder,
  label,
  changeHandler,
  message
}) => {
  let inputElement;
  switch (type) {
    case "checkbox":
      inputElement = (
        <div>
          <div>{message}</div>
          <input type={type} name={name} onChange={changeHandler} /> {label}
          <br />
        </div>
      );
      return inputElement;
    default:
      inputElement = (
        <div>
          <label>{label}</label> <br />
          <div>{message}</div>
          <input
            type={type}
            name={name}
            placeholder={placeholder}
            onChange={changeHandler}
          />
          <br />
        </div>
      );
      return inputElement;
  }
};

export default input;
