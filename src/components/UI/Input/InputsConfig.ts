import { Props } from "./Input";

export interface IInputsConfig {
  [key: string]: InputsConfigType & Value & Rules;
}

type InputsConfigType = Omit<Props, "changeHandler">;

export type Value = { value: string | boolean; validationMessage?: string };

type Rules = {
  rules: { required: boolean; pattern?: RegExp; minLength?: number };
};

const InputsConfig: IInputsConfig = {
  "First name": {
    type: "text",
    name: "firstname",
    placeholder: "Enter your first name",
    label: "First name",
    value: "",
    rules: { required: true, minLength: 1 }
  },
  Lastname: {
    type: "text",
    name: "lastname",
    placeholder: "Enter your lastname",
    label: "Lastname",
    value: "",
    rules: { required: true, minLength: 1 }
  },
  Address: {
    type: "text",
    name: "address",
    placeholder: "Enter your address",
    label: "Address",
    value: "",
    rules: { required: true, minLength: 1 }
  },
  Phone: {
    type: "text",
    name: "phone",
    placeholder: "+0000000000",
    label: "Phone number",
    value: "",
    rules: { required: true, pattern: /^[+][0-9]{10}/ }
  },
  Email: {
    type: "email",
    name: "email",
    placeholder: "example@example.com",
    label: "Email",
    value: "",
    rules: {
      required: true,
      pattern: /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/
    }
  },
  Human: {
    type: "checkbox",
    name: "human",
    label: "I am not a robot",
    value: "",
    rules: { required: true }
  }
};

export default InputsConfig;
